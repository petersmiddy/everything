import unittest
from lambda_ip_location import lambda_handler


class TestGetLocation(unittest.TestCase):
    def test_ip_provided(self):
        event = {
            "queryStringParameters": 
                {
                    "ip": '8.8.8.8'
                }
            }
        res = lambda_handler(event, {})
        
        self.assertEqual(res['statusCode'], 200)

    
    def test_ip_not_provided(self):
        event = {
            "queryStringParameters": {
                "ip": "This should fail..."
            }
        }

        res = lambda_handler(event, {})
        self.assertEqual(res["statusCode"], 400)

    def test_bogus_ip_provided(self):
        event = {
            "queryStringParameters": {
                "ip": "127.0.0.1"
            }
        }

        res = lambda_handler(event, {})
        self.assertEqual(res["statusCode"], 400)


if __name__ == '__main__':
    unittest.main()
