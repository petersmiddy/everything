import unittest
from lambda_forecast import lambda_handler

class TestGetWeather(unittest.TestCase):
    def test_good_lat_long(self):
        mock_api_call = {
        'queryStringParameters': {
                'latitude': '42.8806', 
                'longitude': '71.3273' 
            }   
        }

        res = lambda_handler(mock_api_call, {})
        self.assertEqual(res['statusCode'], 200)

    def test_no_lat_long(self):
        mock_api_call = {
        'queryStringParameters': {
                'no_lat': 'no_lat',
                'no_lon': 'no_lon'
            }   
        }

        res = lambda_handler(mock_api_call, {})
        self.assertEqual(res['statusCode'], 400)



if __name__ == '__main__':
    unittest.main()