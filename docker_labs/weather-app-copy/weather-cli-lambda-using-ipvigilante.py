#! /usr/bin/env python3
import requests
import argparse
import json

DARK_SKY_SECRET_KEY="fbcf15d3d872f2f66720690d60e26343"

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--time', type=int)
args = parser.parse_args()

def get_ip():
    response_body = requests.get("https://www.icanhazip.com")
    return(response_body.text.strip())


def get_location(ip):
    response_body = requests.get("https://ipvigilante.com/" + '?ip='+str(ip))
    loc = response_body.json()['data']
    
    return loc["longitude"], loc["latitude"], loc["city_name"]

def get_forecast(longitude, latitude):
    api_url = 'https://api.darksky.net/forecast/{key}/{lat},{long}'
    url = api_url.format(key=DARK_SKY_SECRET_KEY,
                         lat=latitude,
                         long=longitude)

    if args.time:
        url = url + "," + str(args.time)

    data = requests.get(url)
    #assert data.status_code == 200
    if data.status_code >= 400:
        print(data.status_code)
        print(data.text)
        raise Exception("Error talking to dark sky")
        
    response_dict = data.json()
    forecast = {
        "summary": response_dict.get("currently").get("summary"),
        "temperature": response_dict.get("currently").get("temperature"),
        "humidity": response_dict.get("currently").get("humidity"),
        "apparentTemperature": response_dict.get("currently").get("apparentTemperature"),
    }
    if args.time:
        forecast["high"] = response_dict.get("daily")["data"][0].get("temperatureHigh")
        forecast["low"] = response_dict.get("daily")["data"][0].get("temperatureLow")
    else:
        forecast["high"] = response_dict.get("daily")["data"][0].get("temperatureHigh")
        forecast["low"] = response_dict.get("daily")["data"][0].get("temperatureLow")
    return forecast

def print_forecast(city, forecast, ip):
  print("The weather forecast in " + city)
  print(forecast["summary"])
  print("Temperature: ", forecast['temperature'])
  print("High: ", forecast['high'])
  print("Low: ", forecast['low'])
  print("Humidity: ", forecast['humidity'])
  print("Feels Like: ", forecast['apparentTemperature'])
  print("Your IP Address is: " + str(ip))

if __name__ == "__main__":
  ip = get_ip()
  longitude, latitude, city = get_location(ip)
  forecast = get_forecast(longitude, latitude)
  print_forecast(city, forecast, ip)
  