import json
import requests

def lambda_handler(event, context):
    ip_addr = event['ip']
    api_url = "https://ipvigilante.com/" + ip_addr

    data = requests.get(api_url)
    response_dict = data.json()
    location = {
        "latitude": response_dict["data"]["latitude"],
        "longitude": response_dict["data"]["longitude"],
    }
    return location