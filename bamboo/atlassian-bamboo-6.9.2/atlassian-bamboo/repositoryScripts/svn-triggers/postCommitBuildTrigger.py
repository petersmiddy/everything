#!/usr/bin/python
#
# ./postCommitBuildTrigger.py http://bamoo.atlassian.com/bamboo/ myBuildName

import sys
import urllib2,urllib;

baseUrl = sys.argv[1]
buildKey = sys.argv[2]
query = {'planKey': buildKey}
data = urllib.urlencode(query)



url = baseUrl + "/rest/triggers/latest/remote/changeDetection?planKey=" + buildKey
headers = {'Content-Type': 'application/json'}
request = urllib2.Request(url, headers=headers, data=data)
file = urllib2.urlopen(request).read()

