<html>
<head>
    <meta name="decorator" content="focusTask"/>
    <title>[@s.text name='manual.encryption.page.header'/]</title>
    ${webResourceManager.requireResource('bamboo.web.resources.common:feature-manual-encryption')}
</head>
<body class="aui-page-focused aui-page-size-large">
    [#assign description]
        [@s.text name='manual.encryption.page.description']
            [@s.param value = ctx.helpLink.getUrl('specs.encryption') /]
        [/@s.text]
    [/#assign]
    [@ui.header pageKey='manual.encryption.page.header' description=description/]
    <form id="manual-encryption-form" class="aui"></form>
    <div class="aui-toolbar inline back-button">
        <ul class="toolbar-group">
            <li class="toolbar-item">
                <a href="${req.contextPath}/" class="toolbar-trigger">
                    [@s.text name='manual.encryption.page.back' /]
                </a>
            </li>
        </ul>
    </div>

    <script>
        require(['feature/manual-encryption'], function (ManualEncryption) {
            new ManualEncryption({ el: '#manual-encryption-form' }).render();
        });
    </script>
</body>
</html>
