[#assign cssClass = 'text short-field periodPicker' /]
[#assign periodList = parameters.periodList!['days', 'weeks', 'months'] /]
[#include "/${parameters.templateDir}/${parameters.theme}/controlheader.ftl" /]
[#include "/${parameters.templateDir}/simple/text.ftl" /]
[@s.select
    name=parameters.periodField
    value=parameters.periodValue
    list=periodList
    listKey=parameters.periodListKey!?string
    listValue=parameters.periodListValue!?string
    theme='simple'
    cssClass='select'
    mediumField=true
    disabled=parameters.disabled!false
/]
[#include "/${parameters.templateDir}/${parameters.theme}/controlfooter.ftl" /]
