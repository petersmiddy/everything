[#-- @ftlvariable name="action" type="com.atlassian.bamboo.deployments.environments.actions.ConfigureEnvironmentNotifications" --]
[#import "/lib/build.ftl" as bd]
[#import "/fragments/specs/specsRepositoryLink.ftl" as repoLink/]

[#if readOnly]
    [@ww.text var="title" name="environment.edit.notifications.title.view"]
        [@ww.param][#if environment??]${environment.name?html}[#else]Unknown[/#if][/@ww.param]
    [/@ww.text]
[#else]
    [@ww.text var="title" name="environment.edit.notifications.title.edit"]
        [@ww.param][#if environment??]${environment.name?html}[#else]Unknown[/#if][/@ww.param]
    [/@ww.text]
[/#if]

<html>
<head>
    [@ui.header page=title title=true/]
    <meta name="bodyClass" content="aui-page-focused aui-page-focused-xlarge"/>
</head>
<body>
    <div class="toolbar aui-toolbar inline">[@help.url pageKey="deployments.notifications.howtheywork"][@ww.text name="deployments.notifications.howtheywork.title"/][/@help.url]</div>

    [@ui.header page=title descriptionKey="environment.edit.notifications.description" headerElement="h2"/]

    [#if readOnly]
        [@repoLink.displayRssLinkForDeploymentMessageBox deploymentProject/]
    [/#if]

    [@bd.notificationWarnings /]

    [@ww.url var='addEnvironmentNotificationUrl' value='/chain/admin/ajax/addEnvironmentNotification.action' environmentId=environmentId returnUrl=currentUrl lastModified=lastModified conditionKey=conditionKey notificationRecipientType=notificationRecipientType previousTypeData=previousTypeData saved=true/]

    [#if !readOnly]
        [@cp.displayLinkButton buttonId='addEnvironmentNotification' buttonLabel='chain.config.notifications.add' buttonUrl=addEnvironmentNotificationUrl primary=true/]

        [@dj.simpleDialogForm triggerSelector="#addEnvironmentNotification" width=540 height=420 headerKey="notification.add.title" submitCallback="redirectAfterReturningFromDialog" /]
        [@dj.simpleDialogForm triggerSelector=".edit-notification" width=540 height=420 headerKey="notification.edit.title" submitCallback="redirectAfterReturningFromDialog" /]
    [/#if]

    [@bd.existingNotificationsTable
        notificationSet=existingNotificationsSet
        editUrl='${req.contextPath}/deploy/config/editEnvironmentNotification.action?environmentId=${environmentId}'
        deleteUrl='${req.contextPath}/deploy/config/deleteEnvironmentNotification.action?environmentId=${environmentId}'
        showColumnSpecificHeading=false
        showOperationsColumn=!readOnly
    /]

    <div class="aui-toolbar inline back-button">
        <ul class="toolbar-group">
            <li class="toolbar-item">
                <a id="backToDeploymentProject" href="${req.contextPath}/deploy/config/configureDeploymentProject.action?id=${deploymentProjectId}&environmentId=${environmentId}" class="toolbar-trigger">
                [@ww.text name="deployment.environment.back" /]
                </a>
            </li>
        </ul>
    </div>
</body>
</html>
