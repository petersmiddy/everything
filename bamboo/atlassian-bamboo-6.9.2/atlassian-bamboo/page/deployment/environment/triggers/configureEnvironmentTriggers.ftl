[#-- @ftlvariable name="action" type="com.atlassian.bamboo.deployments.environments.actions.triggers.ConfigureEnvironmentTriggers" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.deployments.environments.actions.triggers.ConfigureEnvironmentTriggers" --]
[#import "/fragments/specs/specsRepositoryLink.ftl" as repoLink/]

[#assign headerText]
    [#if readOnly]
        [@s.text name="deployment.triggers.title.view"][@s.param]${environment.name?html}[/@s.param][/@s.text]
    [#else]
        [@s.text name="deployment.triggers.title.edit"][@s.param]${environment.name?html}[/@s.param][/@s.text]
    [/#if]
[/#assign]

<html>
<head>
    ${webResourceManager.requireResourcesForContext("ace.editor")}
    [@ui.header page=headerText title=true/]
    <meta name="bodyClass" content="aui-page-focused aui-page-focused-xlarge"/>
<body>
</head>
    [#import "/feature/triggers/triggersEditCommon.ftl" as triggers/]
    
    <div class="toolbar aui-toolbar inline">[@help.url pageKey="deployments.triggers.howtheywork"][@ww.text name="deployments.triggers.howtheywork.title"/][/@help.url]</div>
    [@ui.header page=headerText descriptionKey="deployment.triggers.description" headerElement="h2"/]
    
    [#if readOnly]
        [@repoLink.displayRssLinkForDeploymentMessageBox deploymentProject/]
    [/#if]

    [@ww.url var="triggerSelectorUrl" action="viewEnvironmentTriggerTypes" namespace="/deploy/config" environmentId=environmentId/]
    [@ww.url var="editTriggerUrl" action="editEnvironmentTrigger" namespace="/deploy/config" environmentId=environmentId/]
    [@ww.url var="confirmDeleteTriggerUrl" action="confirmDeleteEnvironmentTrigger" namespace="/deploy/config" environmentId=environmentId /]
    [@ww.url var="addTriggerUrl" action="addEnvironmentTrigger" namespace="/deploy/config" environmentId=environmentId returnUrl=currentUrl /]
    
    [@triggers.triggersMainPanel triggers=action.getTriggers() triggerSelectorUrl=triggerSelectorUrl addTriggerUrl=addTriggerUrl editTriggerUrl=editTriggerUrl confirmDeleteTriggerUrl=confirmDeleteTriggerUrl/]
    
    [#if readOnly]
        <script>
            /**
             * @type {module:feature/config-read-only} 
             */
            require('feature/config-read-only').makeReadOnly('#panel-editor-setup');
        </script>
    [/#if]
    
    <div class="aui-toolbar inline back-button">
        <ul class="toolbar-group">
            <li class="toolbar-item">
                <a id="backToDeploymentProject" href="${req.contextPath}/deploy/config/configureDeploymentProject.action?id=${deploymentProject.id}&environmentId=${environment.id}" class="toolbar-trigger">
                    [@ww.text name="deployment.environment.back" /]
                </a>
            </li>
        </ul>
    </div>
</body>
</html>