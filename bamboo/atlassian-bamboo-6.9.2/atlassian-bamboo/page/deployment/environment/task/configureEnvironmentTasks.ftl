[#-- @ftlvariable name="action" type="com.atlassian.bamboo.deployments.environments.actions.ConfigureEnvironmentTasks" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.deployments.environments.actions.ConfigureEnvironmentTasks" --]
[#import "/fragments/specs/specsRepositoryLink.ftl" as repoLink/]

[#assign headerText]
    [#if readOnly]
        [@s.text name="deployment.environment.task.view"][@s.param]${environment.name?html}[/@s.param][/@s.text]
    [#elseif environment.configurationState == 'TASKED']
        [@s.text name="deployment.environment.task.update"][@s.param]${environment.name?html}[/@s.param][/@s.text]
    [#else]
        [@s.text name="deployment.environment.task.create"][@s.param]${environment.name?html}[/@s.param][/@s.text]
    [/#if]
[/#assign]

<html>
<head>
    ${webResourceManager.requireResourcesForContext("ace.editor")}
    [@ui.header page=headerText title=true/]
    <meta name="bodyClass" content="aui-page-focused aui-page-focused-xlarge"/>
</head>
<body>
    [#import "/build/edit/editBuildConfigurationCommon.ftl" as ebcc/]
    [#import "/feature/task/taskConfigurationCommon.ftl" as tcc/]
    [#import "/lib/build.ftl" as bd]
    
    <div class="toolbar aui-toolbar inline">[@help.url pageKey="deployments.tasks.howtheywork"][@ww.text name="deployments.tasks.howtheywork.title"/][/@help.url]</div>
    [@ui.header page=headerText descriptionKey="deployment.environment.task.description" headerElement="h2"/]
    
    [#if readOnly]
        [@repoLink.displayRssLinkForDeploymentMessageBox deploymentProject/]
    [/#if]

    <p class="short-agent-desc">
        [@ww.action name="showAgentNumbers" namespace="/deploy/config" executeResult="true" /]
    </p>
    
    [@ww.url var="taskSelectorUrl" action="viewTaskTypes" namespace="/deploy/config" environmentId=environmentId returnUrl=currentUrl /]
    [@ww.url var="editTaskUrl" action="editEnvironmentTask" namespace="/deploy/config" environmentId=environmentId /]
    [@ww.url var="deleteTaskUrl" action="confirmDeleteEnvironmentTask" namespace="/deploy/config" environmentId=environmentId /]
    [@ww.url var="moveTaskUrl" action="moveTask" namespace="/deploy/config" environmentId=environmentId /]
    [@ww.url var="moveFinalBarUrl" action="moveFinalBar" namespace="/deploy/config" environmentId=environmentId /]
    [@ww.url var="agentAvailabilityUrl" action="showAgentNumbers" namespace="/deploy/config" environmentId=environmentId /]
    
    [@tcc.editTasksCommon taskSelectorUrl agentAvailabilityUrl editTaskUrl deleteTaskUrl moveTaskUrl moveFinalBarUrl/]
      
    [#if readOnly]
        <script>
            /**
             * @type {module:feature/config-read-only}
             */
            require('feature/config-read-only').makeReadOnly('#panel-editor-setup');
        </script>
    [/#if]
    
    <div class="aui-toolbar inline back-button">
        <ul class="toolbar-group">
            <li class="toolbar-item">
                <a id="backToDeploymentProject" href="${req.contextPath}/deploy/config/configureDeploymentProject.action?id=${deploymentProject.id}&environmentId=${environment.id}" class="toolbar-trigger">
                    [#if environment.configurationState == 'CREATED' || environment.configurationState == 'DETAILED']
                        [@ww.text name="deployment.environment.task.finish" /]
                    [#else]
                        [@ww.text name="deployment.environment.back" /]
                    [/#if]
                </a>
            </li>
        </ul>
    </div>
</body>
</html>