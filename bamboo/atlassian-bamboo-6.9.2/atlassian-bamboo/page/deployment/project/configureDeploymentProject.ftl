[#-- @ftlvariable name="action" type="com.atlassian.bamboo.deployments.projects.actions.ConfigureDeploymentProject" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.deployments.projects.actions.ConfigureDeploymentProject" --]
[#import "/fragments/specs/specsRepositoryLink.ftl" as repoLink/]
<html>
<head>
    [#if readOnly]
        [@ui.header title=true object=deploymentProject.name pageKey='deployment.project.configure.readonly.title' /]
    [#else]
        [@ui.header title=true object=deploymentProject.name pageKey='deployment.project.configure.title' /]
    [/#if]
    <meta name="decorator" content="deploymentConfigDecorator"/>
</head>
<body>
[#assign relatedPlanContent]
    [#if action.relatedPlan??]
        [#assign plan =  action.relatedPlan!/]
        [#if fn.hasPlanPermission('READ', plan) ]
            <a href="${req.contextPath}/browse/${plan.planKey.key}">${plan.project.name} &rsaquo; ${plan.buildName}</a>[#t]
        [#else]
            ${plan.project.name} &rsaquo; ${plan.buildName}[#t]
        [/#if]
    [#else]
        [@ww.text name='deployment.project.what.plan.notAvailable'/]
    [/#if]
[/#assign]
    <div id="deployments-configuration-container"></div>
    <script>
        [#assign rssLinkMessage]
            [@repoLink.rssLinkForDeploymentMessage deploymentProject/]
        [/#assign]
        require(['page/deployments-configuration'], function(DeploymentsConfiguration){
            /**
             * @type {DeploymentConfigurationPageOptions}
             */
            var deploymentConfiguration = {
                deploymentProjectId: ${deploymentProject.id},
                canEditDeploymentProject: ${deploymentProject.operations.canEdit?c},
                environments: [],
                deploymentProjectItems: [],
                currentUrl: '${currentUrl?url}',
                repositoryStoredSpecsEnabled: ${featureManager.repositoryStoredSpecsEnabled?c},
                dockerPipelinesEnabled : ${featureManager.dockerPipelinesEnabled?c},
                deploymentPluginsAvailable: ${anyCustomEnvironmentConfigPluginAvailable?c},
                isReadOnly: ${readOnly?c},
                rssRepositoryLinkMessage: '${rssLinkMessage?js_string}',
                el: '#deployments-configuration-container'
            };
            
            [#list environments as environment]
                deploymentConfiguration.environments.push({
                    id: ${environment.id},
                    name: '${environment.name?js_string}',
                    description: '${environment.description!?js_string}',
                    operations: {
                        allowedToExecute: ${environment.operations.allowedToExecute?c},
                        canExecute: ${environment.operations.canExecute?c},
                        canEdit: ${environment.operations.canEdit?c}
                    },
                });
            [/#list]
            
                
            [#if relatedPlan??]
                deploymentConfiguration.relatedPlan = {
                    key: '${relatedPlan.key?js_string}',
                    buildName: '${relatedPlan.buildName?js_string}',
                    project: {
                        name: '${relatedPlan.project.name?js_string}',
                    },
                    planKey: {
                        key: '${relatedPlan.planKey.key?js_string}',
                    }
                };
                
                [#if relatedPlan.description??]
                    deploymentConfiguration.relatedPlan.description = '${relatedPlan.description?js_string}';
                [/#if]
                
                [#if relatedPlan.master??]
                    deploymentConfiguration.relatedPlan.master = {
                        buildName: '${relatedPlan.master.buildName?js_string}'
                    };
                [/#if]
            [/#if]
            
            [#if environmentId??]
                deploymentConfiguration.selectedEnvironmentId = ${environmentId};
            [/#if]
            
            [#list deploymentProjectItems as item]
                deploymentConfiguration.deploymentProjectItems.push({
                    name: '${item.name?js_string}'
                });
            [/#list]

            new DeploymentsConfiguration(deploymentConfiguration).render();
        });
    </script>
</body>
</html>
