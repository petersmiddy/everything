[#-- @ftlvariable name="action" type="com.atlassian.bamboo.deployments.projects.actions.DeploymentProjectAuditLog" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.deployments.projects.actions.DeploymentProjectAuditLog" --]
[@s.text var='title' name='deployment.project.auditLog.title']
    [@s.param][#if deploymentProject??]${deploymentProject.name?html}[#else]Unknown[/#if][/@s.param]
[/@s.text]

<html>
<head>
    [@ui.header page=title title=true /]
    <meta name="bodyClass" content="aui-page-focused aui-page-focused-xlarge"/>
</head>
<body>
    [@ui.header page=title descriptionKey="deployment.project.auditLog.configuration.changes" id="audit_log_header" headerElement="h2" /]
    
    [#if !enabled]
        [@ui.messageBox type="warning"]
            [@s.text name='auditlog.global.disabled.warning'/]
        [/@ui.messageBox]
    [/#if]
    [#if pager.totalSize > 0]
        <p>
            <a href="[@s.url action='deleteDeploymentAuditLog' namespace='/deploy/config' deploymentProjectId=deploymentProjectId/]" class="requireConfirmation mutative" title="[@s.text name='deployment.project.auditLog.delete.confirmation'/]">
                [@s.text name='deployment.project.auditLog.delete' /]
            </a>
        </p>
    [/#if]
    [@s.form cssClass='top-label']
        [@cp.configChangeHistory pager=pager showChangedEntityDetails=true childMap=environmentMap/]

         <div class="buttons-container">
             ${soy.render('aui.buttons.buttons', {
                 'content': soy.render('aui.buttons.button', {
                 'href': '${req.contextPath}/deploy/config/configureDeploymentProject.action?id=${deploymentProjectId}',
                 'text': ' ' + i18n.getText('deployment.project.auditLog.back.text'),
                 'iconClass': 'aui-icon-small aui-iconfont-back-page',
                 'iconType': 'aui'
                 })
             })}
         </div>

        [@s.hidden name='deploymentProjectId' /]
    [/@s.form]
</body>
</html>