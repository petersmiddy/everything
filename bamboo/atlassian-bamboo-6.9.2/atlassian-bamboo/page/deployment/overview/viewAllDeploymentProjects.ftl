[#-- @ftlvariable name="action" type="com.atlassian.bamboo.deployments.projects.actions.ViewAllDeploymentProjects" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.deployments.projects.actions.ViewAllDeploymentProjects" --]
<html>
<head>
    [@ui.header pageKey="deployment.title" title=true/]
    <meta name="decorator" content="deploymentMinimal"/>
</head>

<body>

[#if action.hasDeploymentProject()]
    [#assign headerContent]
        <div id="deployment-project-header-container">
            <h1>[@s.text name='deployment.project.all' /]</h1>
        </div>
    [/#assign]

    [#assign content]
        <div id="deployment-project-container"></div>
        <script>
            require(['page/deployments-dashboard'], function(DeploymentsDashboard) {
                new DeploymentsDashboard({
                    el: '#deployment-project-container',
                    headerEl: '#deployment-project-header-container'
                }).render();
            });
        </script>
    [/#assign]

    ${soy.render("bamboo.deployments:deployments-main", "bamboo.layout.deployment", {
        "headerContent": headerContent,
        "content": content
    })}

[#else]
    ${webResourceManager.requireResourcesForContext("atl.dashboard")}

    ${soy.render("bamboo.deployments:deployments-main", "bamboo.layout.deployment", {
                 "headerText": action.getText("deployment.welcome.heading"),
                 "content": soy.render('bamboo.feature.dashboard.welcomeMat.welcomeMessageDeploy', {
                                       "hasGlobalCreatePermission": fn.hasGlobalPermission('CREATE'),
                                       "hasBuilds": ctx.hasBuilds()
                            })
    })}
[/#if]

<script>
    AJS.trigger('analytics',
        {
            name: 'bamboo.page.visited.deployment.dashboard',
            data: {
                user: '${hashedUser?js_string}'
            }
        }
    );
</script>
</body>
</html>
