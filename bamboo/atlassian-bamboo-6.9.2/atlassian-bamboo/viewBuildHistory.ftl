[#-- @ftlvariable name="action" type="com.atlassian.bamboo.build.ViewBuild" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.build.ViewBuild" --]
[#if user??]
    [#assign rssSuffix="&amp;os_authType=basic" /]
[#else]
    [#assign rssSuffix="" /]
[/#if]

[#assign planName=buildKey/]
[#if immutablePlan?has_content]
    [#assign planName=immutablePlan.name/]
[/#if]

<html>
<head>
    <title>${planName}: Builds</title>
    <link rel="alternate" type="application/rss+xml" title="Bamboo RSS feed" href="${req.contextPath}/rss/createAllBuildsRssFeed.action?feedType=rssAll&buildKey=${buildKey}${rssSuffix}" />
    <meta name="tab" content="results"/>
</head>

<body>
    [@ui.header pageKey='build.results.title' /]
    [@ww.action name="viewBuildResultsTable" namespace="/build" executeResult="true" ]
        [@ww.param name="showAgent" value="true"/]
        [@ww.param name="sort" value="true"/]
        [@ww.param name="singlePlan" value="true"/]
    [/@ww.action]
    <ul>
        <li>Successful builds are <font color="green">green</font>, failed builds are <font color="red">red</font>.</li>
        [#if immutablePlan?has_content]
            <li>This plan has been built ${immutablePlan.lastBuildNumber} times.</li>
            [#if immutablePlan.averageBuildDuration??]
                <li>The average build time for recent builds is approximately ${durationUtils.getPrettyPrint(immutablePlan.averageBuildDuration)}</li>
            [/#if]
        [/#if]

        <li>
            <a href="${req.contextPath}/rss/createAllBuildsRssFeed.action?feedType=rssAll&buildKey=${buildKey}${rssSuffix}">[@ui.icon useIconFont=true type="rss" text="Point your rss reader at this link to get the full ${planName} build feed" /]</a>
            Feed for all <a href="${req.contextPath}/rss/createAllBuildsRssFeed.action?feedType=rssAll&buildKey=${buildKey}${rssSuffix}" title="Point your rss reader at this link to get the full ${planName} build feed">builds</a>
            or just the <a href="${req.contextPath}/rss/createAllBuildsRssFeed.action?feedType=rssFailed&buildKey=${buildKey}${rssSuffix}" title="Point your rss reader at this link to get just the failed ${planName} build feed">failed builds</a>.
        </li>
    </ul>
</body>
</html>
