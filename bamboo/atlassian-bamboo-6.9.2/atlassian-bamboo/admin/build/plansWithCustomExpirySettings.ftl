[#-- @ftlvariable name="" type="com.atlassian.bamboo.build.expiry.PlansWithCustomExpirySettings" --]
[#-- @ftlvariable name="action" type="com.atlassian.bamboo.build.expiry.PlansWithCustomExpirySettings" --]
<html>
<head>
    <title>[@s.text name='buildExpiry.plans.with.custom.settings.title' /]</title>
    <meta name="decorator" content="adminpage">
    <meta name="adminCrumb" content="buildExpiry">
</head>
<body>
<ol class="aui-nav aui-nav-breadcrumbs plan-expiry-admin-breadcrumbs" style="float: left">
    <li>
        <a href="[@ww.url action='buildExpiry.action' namespace='/admin' /]">[@s.text name='buildExpiry.title' /]</a>
    </li>
    <li>
        [@s.text name='buildExpiry.plans.with.custom.settings.title'/]
    </li>
</ol>
    <div id="plans-with-custom-expiry-settings-container"></div>
    <script>
        require(['feature/plans-with-custom-expiry-settings'], function(PlansWithCustomExpirySettings) {
            new PlansWithCustomExpirySettings({
                el: '#plans-with-custom-expiry-settings-container',
                globalExpirySettings: {
                    expiryNothing: ${expiryNothing!?c},
                    expiryResults: ${expiryResults!?c},
                    expiryArtifacts: ${expiryArtifacts!?c},
                    expiryLogs: ${expiryBuildLogs!?c},
                    duration: ${duration!0},
                    period: '${period!?js_string}',
                    buildsToKeep: ${buildsToKeep!0},
                    labelsList: '${labels!?js_string}'
                }
            }).render();
        });
</script>
</body>
</html>