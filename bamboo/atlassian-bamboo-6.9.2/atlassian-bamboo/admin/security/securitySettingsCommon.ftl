[#-- @ftlvariable name="action" type="com.atlassian.bamboo.configuration.ConfigureSecurity" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.configuration.ConfigureSecurity" --]

[#macro rssSecuritySettings isReadOnly]
    <h3>[@s.text name='config.security.rss.title' /] [@help.icon pageKey='security.rss' /]</h3>
    [@ui.bambooSection]
        [@s.checkbox labelKey='config.security.rss.enable' name='rssEnabled' toggle=true disabled=isReadOnly/]

        [@ui.bambooSection dependsOn='rssEnabled' showOn=true ]

            [#-- use docker --]
            [@s.checkbox labelKey='config.security.rss.execute.specs.in.docker' name='rssExecuteSpecsInDocker' toggle=true disabled=isReadOnly/]
            [@ui.bambooSection dependsOn='rssExecuteSpecsInDocker' showOn=true ]

                [#if !dockerConfigured]
                    [@ww.text var='rssMissingDocker' name='config.security.rss.docker.not.configured']
                        [@ww.param][@ww.url action='configureSharedLocalCapabilities' namespace='/admin/agent'/][/@ww.param]
                        [@ww.param][@help.href pageKey='security.rss'/][/@ww.param]
                    [/@ww.text]
                    <div class="field-group">
                        [@ui.messageBox type='warning' content=rssMissingDocker/]
                    </div>
                [/#if]

                [@s.textfield name='rssDockerImage' labelKey='config.security.rss.docker.image' cssClass='long-field'
                    required=true disabled=isReadOnly description=action.rssDockerImageDescription /]

                [@s.checkbox labelKey='config.security.rss.rssMountLocalMavenDirectory' name='rssMountLocalMavenDirectory' toggle=true disabled=isReadOnly/]
                [@ui.bambooSection dependsOn='rssMountLocalMavenDirectory' showOn=true]
                    [@s.textfield name='rssLocalMavenDirectory' labelKey='config.security.rss.rssLocalMavenDirectory' cssClass='long-field' required=true disabled=isReadOnly/]
                [/@ui.bambooSection]
            [/@ui.bambooSection]

        [/@ui.bambooSection]
    [/@ui.bambooSection]

[/#macro]

[#macro systemEncryptionSettings isReadOnly]
    <h3>[@s.text name='config.security.encryption.title' /] [@help.icon pageKey='security.system.encryption' /]</h3>
    [@ui.bambooSection]
        [@s.checkbox labelKey='config.security.manual.encryption.enabled' name='manualEncryptionEnabled' toggle=true disabled=isReadOnly/]

        [@ui.bambooSection dependsOn='manualEncryptionEnabled' showOn=true ]
            [@s.textfield
                name='manualEncryptionLimit'
                labelKey='config.security.manual.encryption.limit.invocations'
                template='periodPicker'
                periodField='manualEncryptionLimitUnit'
                periodValue=stack.findValue('manualEncryptionLimitUnit')!
                periodList=action.supportedManualEncryptionTimeUnits
                periodListKey='name'
                periodListValue='label'
                required=true
                disabled=isReadOnly /]
        [/@ui.bambooSection]
    [/@ui.bambooSection]

[/#macro]
