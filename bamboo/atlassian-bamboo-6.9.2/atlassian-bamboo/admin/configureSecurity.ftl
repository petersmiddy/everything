[#-- @ftlvariable name="action" type="com.atlassian.bamboo.configuration.ConfigureSecurity" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.configuration.ConfigureSecurity" --]
[#include "/admin/security/securitySettingsCommon.ftl"/]

<html>
<head>
    [@ui.header pageKey="config.security.heading" title=true/]
    <meta name="adminCrumb" content="configureSecurityPage">
</head>
<body>
    [@ui.header pageKey="config.security.heading" descriptionKey="config.security.description"/]
    [@s.form action="saveConfigureSecurity"
              id="configurationSecurityForm"
              class="security-settings-form"
              submitLabelKey='global.buttons.update'
              cancelUri='/admin/viewSecurity.action']

        [#-- general settings --]
        <h3>[@s.text name='config.security.form.edit.heading'/]</h3>
        [@ui.bambooSection]
            [@s.checkbox id='enableSignup_id' labelKey='config.security.enableSignup' toggle=true name='enableSignup'/]
            [@ui.bambooSection dependsOn='enableSignup' showOn=true ]
                [@s.checkbox id='enableCaptchaOnSignup_id' labelKey='config.security.enableCaptchaOnSignup' name='enableCaptchaOnSignup'/]
            [/@ui.bambooSection]
            [@s.checkbox id='enableViewContactDetails_id' labelKey='config.security.enableViewContactDetails' descriptionKey='config.security.enableViewContactDetails.description' name='enableViewContactDetails'/]
            [@s.checkbox id='enableRestrictedAdmin_id' labelKey='config.security.enableRestrictedAdmin' descriptionKey='config.security.enableRestrictedAdmin.description' name='enableRestrictedAdmin'/]

            [#if featureManager.soxComplianceModeConfigurable]
                [@s.checkbox labelKey='config.security.soxComplianceMode' name='soxComplianceModeEnabled' /]
            [/#if]

            [@s.checkbox id='enableCaptcha_id' labelKey='config.security.enableCaptcha' toggle=true name='enableCaptcha'/]
            [@ui.bambooSection dependsOn='enableCaptcha' showOn=true]
                [@s.textfield id='loginAttempts_id' labelKey='config.security.loginAttempts' name="loginAttempts" required=true/]
            [/@ui.bambooSection]
            [@s.checkbox labelKey='config.security.xsrfProtection' name='xsrfProtectionEnabled' toggle=true/]
            [@ui.bambooSection dependsOn='xsrfProtectionEnabled' showOn=true]
                [@s.checkbox labelKey='config.security.xsrfProtection.mutativeGetsAllowed' name='xsrfProtectionMutativeGetsAllowed'/]
            [/@ui.bambooSection]
            [@s.checkbox labelKey='config.security.resolveArtifactContentTypeByExtension' name='resolveArtifactContentTypeByExtension' /]
            [@s.checkbox labelKey='config.security.manageTrustedKeys' name='manageAcceptedSshHostKeys']
                [@s.param name="description"]
                    [#if manageAcceptedSshHostKeys]
                        [@s.text name="config.security.manageTrustedKeys.enabled.description"]
                            [@s.param][@s.url namespace='/admin' action='trustedKeys'/][/@s.param]
                        [/@s.text]
                    [#else ]
                        [@s.text name="config.security.manageTrustedKeys.disabled.description" /]
                    [/#if]
                [/@s.param]
            [/@s.checkbox]
            [@s.checkbox labelKey='config.security.showAdminContactDetailsToAnonymousUsers' name='showAdminContactDetailsToAnonymousUsers' /]
            [@s.checkbox labelKey='config.security.repository.trigger.anonymous' name='unauthenticatedRemoteTriggerAllowed' /]
        [/@ui.bambooSection]

        [#-- serialization protection methods --]
        <h3>[@s.text name='config.security.serialization.protection.method' /] [@help.icon pageKey='security.serialization.protection.method' /]</h3>
        [@ui.bambooSection]
            [@s.select id='xstreamSerializationProtectionMethod_id'
                cssClass="labelForCheckbox" labelKey='config.security.xstream.serialization.protection.method'
                name="xstreamSerializationProtectionMethod"
                listKey="key"
                listValue="value"
                list=serializationProtectionOptionsForRemoting /]
            [@s.select id='bandanaSerializationProtectionMethod_id' cssClass="labelForCheckbox" labelKey='config.security.bandana.serialization.protection.method' name="bandanaSerializationProtectionMethod"
                listKey="key"
                listValue="value"
                list=serializationProtectionOptionsForBandana/]
        [/@ui.bambooSection]

        [#-- repository stored specs security settings --]
        [#if featureManager.repositoryStoredSpecsEnabled ]
            [@rssSecuritySettings isReadOnly=false /]
        [/#if]

        [#-- system-wide encryption security settings --]
        [@systemEncryptionSettings isReadOnly=false/]
    [/@s.form]
</body>
</html>
