[#import "/lib/build.ftl" as bd]
[@ww.form action='addSystemNotification'
    namespace='/admin'
    showActionErrors='false'
    submitLabelKey='global.buttons.add'
    id='systemNotificationForm']
    [@ww.hidden name="returnUrl" /]
    [@bd.commonNotificationFormContent  /]
[/@ww.form]