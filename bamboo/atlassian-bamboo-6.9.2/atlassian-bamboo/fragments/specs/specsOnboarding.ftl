[#-- @ftlvariable name="action" type="com.atlassian.bamboo.webwork.StarterAction" --]
[#-- @ftlvariable name="" type="com.atlassian.bamboo.webwork.StarterAction" --]

[#if specsOnboarding.visible]
    [#assign webhookSetupDocumentationUrl][@help.href pageKey="bamboo.specs.webhook.setup" /][/#assign]
    <script type="text/javascript">
        require(['feature/specs-onboarding-dialog'], function(SpecsOnboardingDialog) {
            new SpecsOnboardingDialog({
                                          repositoryId: ${specsOnboarding.specsRepositoryId},
                                          requiresWebhook: ${specsOnboarding.specsWebhookRequired?string},
                                          bambooBaseUrl: '${baseUrl?js_string}',
                                          bambooSpecsVersion: '${specsOnboarding.bambooSpecsVersion?js_string}',
                                          webhookSetupDocumentationUrl: '${webhookSetupDocumentationUrl?js_string}',
                                          showSecuritySettingsHint: ${specsOnboarding.showSecuritySettingsHint?c},
                                          onClose: function() {
                                              // remove query params to avoid displaying the dialog on page refresh
                                              window.history.replaceState(null, null, window.location.pathname);
                                          },
                                          [#if specsOnboarding.buildProjectKey??]
                                              buildProjectKey: '${specsOnboarding.buildProjectKey?js_string}',
                                          [/#if]
                                      }).show();
        });
    </script>
[/#if]