[#-- Returns data to be passed to base Bamboo layout SOY template, to render admin panel horizontal navigation. --]
[#function horizontalMenuData sections]
[#-- @ftlvariable name="sections" type="java.util.List<com.atlassian.bamboo.ww2.FreemarkerContext.WebSectionDto>" --]
    [#local navItems = [] /]
    [#if fn.hasAdminPermission()]
        [#list sections as section]
            [#local webSection = section.webSection /]
            [#local items = section.items]
            [#if items?has_content]
                [#local firstWebItem = items.get(0).webItem /]
                [#local navItems = navItems + [{
                    "content": webSection.label!,
                    "linkId": getWebFragmentKey(webSection, true)!,
                    "href": firstWebItem.url!'#',
                    "isSelected": section.active
                }]]
            [/#if]
        [/#list]
    [/#if]
    [#return navItems /]
[/#function]

[#-- Renders admin panel vertical navigation for currently active web section. --]
[#macro verticalMenuContent sections]
[#-- @ftlvariable name="sections" type="java.util.List<com.atlassian.bamboo.ww2.FreemarkerContext.WebSectionDto>" --]
    [#if fn.hasAdminPermission()]
        <nav id="admin-menu" class="aui-navgroup aui-navgroup-vertical">
            <div class="aui-navgroup-inner">
                [#list sections as section]
                    [#local webSection = section.webSection /]
                    [#if sections?size > 1 && webSection.label?has_content]
                        <div class="aui-nav-heading"><strong>${webSection.label}</strong></div>
                    [/#if]
                    <ul class="aui-nav">
                        [#list section.items as item]
                            [#local webItem = item.webItem /]
                            <li class="[#if item.active]aui-nav-selected[/#if]">
                                <a id="${getWebFragmentKey(webItem)!}" href="${webItem.url!'#'}">
                                    ${webItem.label!}
                                </a>
                            </li>
                        [/#list]
                    </ul>
                [/#list]
            </div>
        </nav>
    [/#if]
[/#macro]

[#--
    Obtain a web fragment's unique key, either from its 'id' property or by extracting chunks from its 'completeKey'.
    If the 'prependPluginKey' argument is set to true, the method will try to prepend plugin's key to the result,
    separating app's key and fragment's key with a hyphen.

    Examples:
        id = 'X',  completeKey = 'Y:Z', result = 'X' or 'Y-X'
        id = null, completeKey = 'Y:Z', result = 'Z' or 'Y-Z'
        id = 'X',  completeKey = null,  result = 'X'
        id = null, completeKey = null,  result = null
--]
[#function getWebFragmentKey webFragment prependPluginKey=false]
[#-- @ftlvariable name="webFragment" type="com.atlassian.plugin.web.api.WebFragment" --]
[#-- @ftlvariable name="appendPluginKey" type="java.lang.Boolean" --]
    [#local result = (webFragment.id!((webFragment.completeKey!'')?keep_after_last(':')))! /]
    [#if result?has_content && prependPluginKey && webFragment.completeKey?has_content]
        [#local result = webFragment.completeKey?keep_before_last(':') + '-' + result /]
    [/#if]
    [#return result /]
[/#function]
