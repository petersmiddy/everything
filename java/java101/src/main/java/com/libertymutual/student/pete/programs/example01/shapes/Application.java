package com.libertymutual.student.pete.programs.example01;

import java.awt.Color;
import java.math.BigDecimal;
import com.libertymutual.student.pete.programs.example01.shapes.Circle;
import com.libertymutual.student.pete.programs.example01.shapes.Square;

public class Application {

    public static void main(String[] args) {

        int radius = 10;
        Circle circle = new Circle(radius, Color.PINK);
        System.out.println(circle.getArea());

        int length = 100;
        Square square = new Square(length, Color.RED);
        System.out.println(square.getArea());
    }
}
