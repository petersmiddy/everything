package com.libertymutual.student.pete.programs.example01.shapes;

import org.junit.Test;
import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CircleTest {

    @Test
    public void testGetArea() {
        Circle circle = new Circle(10, Color.red);
        BigDecimal area = circle.getArea();
        BigDecimal expectedAnswer = new BigDecimal(314);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }
    @Test
    public void testGetColor() {
        Square square = new Square(100, Color.pink);
        Color color = square.getColor();
        Color expectedColor = Color.pink;
        assertEquals("Verify that the color is correct", expectedColor, color);
    }
    }
